import React from "react";
import { VehicleFilter, VehicleType } from "../data/vehicles/contracts";
import { VehicleTypeSelect } from "./VehicleTypeSelect";



export const Filter = (props: { onChange: (arg: any) => void; filter: VehicleFilter; }) => {
  

    const handleTitleChange = (title: string | null): void => {
        props.onChange({ ...props.filter, title });
      };
    
    const handleTypeChange = (type: VehicleType | null): void => {
        props.onChange({ ...props.filter, type });
      };

    return (
      <div>
        <input
          value={props.filter.title}
          onChange={e => handleTitleChange(e.target.value)}
        />
        <VehicleTypeSelect
          value={props.filter.type}
          onChange={handleTypeChange}
        />
      </div>
    );
  }

  

